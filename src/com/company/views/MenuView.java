package com.company.views;
import com.company.controlers.Background;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

import java.awt.*;

public class MenuView {
    private Font awtFont, awtFontSmall;
    private TrueTypeFont font, fontSmall;
    private org.newdawn.slick.Image gecko, gecko2, bubble;
    private Background background;

    private long startTime, startTime2;
    private long elapsedTime = 0L, elapsedTime2 = 0L;
    private float yText = 200;
    private float x=500 ,y = 200;
    private boolean shake = false;
    private boolean goUpText = true;
    private boolean blinking = false;
    private int lastScore = 0;
    private int highScore = 0;
    private int sizeGecko =400;
    private int sizeBubble =sizeGecko+200;
    private float vX=0.2f;
    private float vY=0.15f;

    private static final String pathGecko = "res/geckoBig.png";
    private static final String pathGecko2 = "res/geckoBig2.png";
    private static final String pathBubble = "res/bubble.png";

    public MenuView(int lastScore, int highScore) throws SlickException {
        awtFont = new Font("Comic Sans MS", Font.PLAIN, 50);
        awtFontSmall = new Font("Comic Sans MS", Font.PLAIN, 30);
        font = new TrueTypeFont(awtFont, true);
        fontSmall = new TrueTypeFont(awtFontSmall, true);
        startTime = System.currentTimeMillis();
        startTime2 = System.currentTimeMillis();
        gecko = new org.newdawn.slick.Image(pathGecko);
        gecko2 = new org.newdawn.slick.Image(pathGecko2);
        bubble = new org.newdawn.slick.Image(pathBubble);
        background = new Background();
        this.lastScore=lastScore;
        this.highScore=highScore;
    }

    public void render(org.newdawn.slick.Graphics graphics){
        background.render();
        if(blinking){
            gecko2.draw(x+100,y+100, sizeGecko,sizeGecko);
        }
        else{
            gecko.draw(x+100,y+100,sizeGecko,sizeGecko);
        }
        bubble.draw(x,y,400+200,sizeBubble);
        graphics.setFont(font);
        graphics.setColor(org.newdawn.slick.Color.red);
        graphics.drawString("^^   J  U  M  P        G  A  M  E  ^^", 580,75);
        graphics.setColor(org.newdawn.slick.Color.white);
        if(lastScore!=0){
            graphics.drawString("YOUR SCORE:  "+lastScore, 200,200);
        }
        if(highScore!=0){
            graphics.setColor(org.newdawn.slick.Color.lightGray);
            graphics.drawString("HIGH SCORE:  "+highScore, 200,280);
            graphics.setColor(org.newdawn.slick.Color.white);
        }
        graphics.drawString("PRESS  >>SPACE<<  TO PLAY", 200, yText +600);
        graphics.drawString("PRESS    >>Q<<    TO QUIT", 200,900);
        graphics.setFont(fontSmall);
        graphics.drawString("CONTROLS:", 1200,800);
        graphics.drawString("ARROW LEFT       2 GO LEFT", 1200,850);
        graphics.drawString("ARROW RIGHT    2 GO RIGHT", 1200,900);
        graphics.drawString("ARROW DOWN    2 DASH DOWN", 1200,950);
    }

    public void update( int t){

        if(elapsedTime < 2*1000){
            elapsedTime = System.currentTimeMillis() - startTime;
        }
        else{
            shake=true;
            startTime = System.currentTimeMillis();
            elapsedTime = 0;
        }
        if(!blinking){
            if(elapsedTime2 < 3*1000){
                elapsedTime2 = System.currentTimeMillis() - startTime2;
            }
            else{
                blinking=true;
                startTime2 = System.currentTimeMillis();
                elapsedTime2 = 0;
            }
        }
        else{
            if(elapsedTime2 < 0.4*1000){
                elapsedTime2 = System.currentTimeMillis() - startTime2;
            }
            else{
                blinking=false;
                startTime2 = System.currentTimeMillis();
                elapsedTime2 = 0;
            }
        }


        if(shake){
            if(goUpText){
                if(yText >180){
                    yText -=t*0.1;
                }
                else{
                    goUpText =false;
                }
            }
            else{
                if(yText <200){
                    yText +=t*0.1;
                }
                else{
                    yText = 200;
                    goUpText =true;
                    shake=false;
                }
            }
        }
        if(x>1920-sizeBubble){
            vX=-1*vX;
            x=1920-sizeBubble;
        }
        if(x<0){
            vX=-1*vX;
            x=0;
        }
        if(y>1080-sizeBubble){
            vY=-1*vY;
            y=1080-sizeBubble;
        }
        if(y<0){
            vY=-1*vY;
            y=0;
        }
        x+=vX*t;
        y+=vY*t;
    }
}
