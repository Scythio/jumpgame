package com.company.views;

import com.company.controlers.PlatformInterface;
import com.company.models.*;

public class PlatformView extends View implements PlatformInterface {

    private static final String PATH_BIG_PLATFORM = "res/bigPlatform.png";
    private static final String PATH_SMALL_PLATFORM = "res/smallPlatform.png";
    private static final String PATH_MOVING_PLATFORM = "res/movingPlatform.png";
    private static final String PATH_SMALL_MOVING_PLATFORM = "res/smallMovingPlatform.png";
    private static final String PATH_BREAKABLE_PLATFORM_3 = "res/breakablePlatform3.png";
    private static final String PATH_BREAKABLE_PLATFORM_2 = "res/breakablePlatform2.png";
    private static final String PATH_BREAKABLE_PLATFORM_1 = "res/breakablePlatform1.png";
    private static final String PATH_SMALL_BREAKABLE_PLATFORM_2 = "res/smallBreakablePlatform2.png";
    private static final String PATH_SMALL_BREAKABLE_PLATFORM_1 = "res/smallBreakablePlatform1.png";
    private static final String PATH_MIX_TRAMPOLINE = "res/trampolineMix.png";
    private static final String PATH_SOLID_TRAMPOLINE = "res/trampolineSolid.png";
    private static final String PATH_MOVING_TRAMPOLINE = "res/trampolineMoving.png";
    private static final String PATH_BREAKABLE_TRAMPOLINE = "res/trampolineBreakable.png";

    public PlatformView(){
        super();
        addTexture(PATH_BIG_PLATFORM, BigPlatform.WIDTH,BigPlatform.HEIGHT,Type.BIG_PLATFORM);
        addTexture(PATH_SMALL_PLATFORM,SmallPlatform.WIDTH,SmallPlatform.HEIGHT,Type.SMALL_PLATFORM);
        addTexture(PATH_MOVING_PLATFORM , MovingPlatform.WIDTH,MovingPlatform.HEIGHT,Type.MOVING_PLATFORM);
        addTexture(PATH_SMALL_MOVING_PLATFORM ,SmallMovingPlatform.WIDTH,SmallMovingPlatform.HEIGHT,Type.SMALL_MOVING_PLATFORM);
        addTexture(PATH_BREAKABLE_PLATFORM_3, BreakablePlatform.WIDTH,BreakablePlatform.HEIGHT,Type.BREAKABLE_PLATFORM_THREE_LAYERS);
        addTexture(PATH_BREAKABLE_PLATFORM_2,BreakablePlatform.WIDTH,BreakablePlatform.HEIGHT,Type.BREAKABLE_PLATFORM_TWO_LAYERS);
        addTexture(PATH_BREAKABLE_PLATFORM_1,BreakablePlatform.WIDTH,BreakablePlatform.HEIGHT,Type.BREAKABLE_PLATFORM_ONE_LAYER);
        addTexture(PATH_SMALL_BREAKABLE_PLATFORM_2, SmallBreakablePlatform.WIDTH,SmallBreakablePlatform.HEIGHT,Type.SMALL_BREAKABLE_PLATFORM_TWO_LAYERS);
        addTexture(PATH_SMALL_BREAKABLE_PLATFORM_1,SmallBreakablePlatform.WIDTH,SmallBreakablePlatform.HEIGHT,Type.SMALL_BREAKABLE_PLATFORM_ONE_LAYER);
        addTexture(PATH_MIX_TRAMPOLINE,Trampoline.WIDTH,Trampoline.HEIGHT,Type.MIX_TRAMPOLINE);
        addTexture(PATH_SOLID_TRAMPOLINE,Trampoline.WIDTH,Trampoline.HEIGHT,Type.SOLID_TRAMPOLINE);
        addTexture(PATH_MOVING_TRAMPOLINE,Trampoline.WIDTH,Trampoline.HEIGHT,Type.MOVING_TRAMPOLINE);
        addTexture(PATH_BREAKABLE_TRAMPOLINE,Trampoline.WIDTH,Trampoline.HEIGHT,Type.BREAKABLE_TRAMPOLINE);
    }
    public void render(Platform platform){
        render(platform.getX(),platform.getY(),platform.getShiftY(),platform.getType());
    }
    public void render(float x, float y, float shiftY, Type type){
        super.render(x,y+shiftY,idOfType(type));
    }
}
