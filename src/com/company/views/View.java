package com.company.views;
import com.company.controlers.PlatformInterface;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.ArrayList;

public class View implements PlatformInterface {

    private class Texture {
        private Image image;
        private boolean hasImage;
        private int id;
        private float width, height;

        public Texture(Image image, float width, float height, int id) {
            this.image = image;
            this.width = width;
            this.height = height;
            this.hasImage = true;
            this.id = id;
        }

        public Texture(float width, float height, int id) {
            this.width = width;
            this.height = height;
            this.hasImage = false;
            this.id = id;
        }

        public Texture(Image image, float width, float height, Type type) {
            this(image,width,height,idOfType(type));
        }

        public Texture(float width, float height, Type type) {
            this(width,height,idOfType(type));
        }


        public void draw(float x, float y){
            if(hasImage){
                image.draw(x,y,width,height);
            }
            else{
                graphics.fillRect(x,y,width,height);
            }
        }
        public void draw(float x, float y, float width, float height){
            if(hasImage){
                image.draw(x,y,width,height);
            }
            else{
                graphics.fillRect(x,y,width,height);
            }
        }
    }

    private ArrayList<Texture> allTextures;
    private int numberOfTextures = 0;
    private boolean hasTexture;
    private Graphics graphics = new Graphics();

    public View(){
        allTextures = new ArrayList<Texture>();
        hasTexture = false;
    }

    public View(float width, float height, String path) {
        allTextures = new ArrayList<Texture>();
        this.addTexture(path,width,height,1);
        setCenterOfRotation();
    }

    public View(float width, float height, String path1, String path2) {
        allTextures = new ArrayList<Texture>();
        this.addTexture(path1,width,height,1);
        this.addTexture(path2,width,height,2);
        setCenterOfRotation();
    }
    public View(float width, float height, String path1, String path2, String path3) {
        allTextures = new ArrayList<Texture>();
        this.addTexture(path1,width,height,1);
        this.addTexture(path2,width,height,2);
        this.addTexture(path3,width,height,3);
        setCenterOfRotation();
    }

    public void addTexture(String path, float width, float height, int id) {
        try{
            allTextures.add(new Texture(new Image(path),width,height,id));
            hasTexture = true;
        } catch (SlickException e){
            allTextures.add(new Texture(width,height,id));
            e.printStackTrace();
        }
        numberOfTextures++;
    }

    public void addTexture(String path, float width, float height, Type type) {
        try{
            allTextures.add(new Texture(new Image(path),width,height,type));
            hasTexture = true;
        } catch (SlickException e){
            allTextures.add(new Texture(width,height,type));
            e.printStackTrace();
        }
        numberOfTextures++;
    }

    public void render(float x, float y){
        if(!allTextures.isEmpty()){
            allTextures.get(0).draw(x, y);
        }
    }

    public void render(float x, float y, float width, float height){
        if(!allTextures.isEmpty()){
            allTextures.get(0).draw(x, y, width, height);
        }
    }

    public void render(float x, float y, int id){
        if(!allTextures.isEmpty()){
            if( id>0 && id<=numberOfTextures){
                allTextures.get(id-1).draw(x, y);
            }
            else{
                allTextures.get(0).draw(x, y);
            }
        }
    }

    public void setCenterOfRotation(){
        if(hasTexture){
            for(Texture texture: allTextures){
                texture.image.setCenterOfRotation(texture.width / 2, texture.height / 2);
            }
        }
    }

    public void setRotation(float alpha){
        if(hasTexture){
            for(Texture texture: allTextures){
                texture.image.setRotation(alpha);
            }
        }
    }

    public int idOfType(Type type) {
        switch (type) {
            case BIG_PLATFORM:
                return 1;
            case SMALL_PLATFORM:
                return 2;
            case MOVING_PLATFORM:
                return 3;
            case SMALL_MOVING_PLATFORM:
                return 4;
            case BREAKABLE_PLATFORM_THREE_LAYERS:
                return 5;
            case BREAKABLE_PLATFORM_TWO_LAYERS:
                return 6;
            case BREAKABLE_PLATFORM_ONE_LAYER:
                return 7;
            case SMALL_BREAKABLE_PLATFORM_TWO_LAYERS:
                return 8;
            case SMALL_BREAKABLE_PLATFORM_ONE_LAYER:
                return 9;
            case MIX_TRAMPOLINE:
                return 10;
            case SOLID_TRAMPOLINE:
                return 11;
            case MOVING_TRAMPOLINE:
                return 12;
            case BREAKABLE_TRAMPOLINE:
                return 13;
            default:
                return 0;
        }
    }
}