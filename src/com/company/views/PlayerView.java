package com.company.views;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class PlayerView extends View {

    private static final float WIDTH = 80;
    private static final float HEIGHT = 80;
    private static final String PATH_MIDDLE = "res/geckoM.png";
    private static final String PATH_DOWN = "res/geckoD.png";
    private static final String PATH_UP = "res/geckoU.png";
    private static final String PATH_SHIELD = "res/shield.png";
    private static Image shield;
    private boolean hasShieldTexture;

    public PlayerView() {
        super(WIDTH, HEIGHT, PATH_MIDDLE, PATH_DOWN, PATH_UP);
        try{
            shield = new Image(PATH_SHIELD);
            hasShieldTexture = true;
        } catch (SlickException e){
            e.printStackTrace();
            hasShieldTexture = false;
        }

    }

    public void render(float x, float y, int ID, boolean shieldOn){
        super.render(x-10, y-10,ID);
        if(hasShieldTexture){
            if(shieldOn){
                shield.draw(x-30, y-30,WIDTH+40,HEIGHT+40);
            }
        }
    }
}
