package com.company.views;

import com.company.models.Enemy;

public class EnemyView extends View {

    private static final String PATH_ENEMY = "res/enemy.png";

    public EnemyView(){
        this(Enemy.WIDTH,Enemy.HEIGHT);
    }

    public EnemyView(float width, float height){
        super(width,height,PATH_ENEMY);
    }

    public void render(Enemy enemy){
        this.render(enemy.getX(),enemy.getY(),enemy.getShiftY());
    }

    public void render(float x,float y, float shiftY){
        super.render(x,y+shiftY);
    }
}
