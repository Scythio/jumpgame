package com.company.views;

import com.company.models.Star;

public class StarView extends View {

    protected static float WIDTH = 20;
    protected static float HEIGHT = 20;
    private static final String PATH_STAR = "res/star.png";

    public StarView(){
        super(WIDTH,HEIGHT,PATH_STAR);
    }

    public void render(Star star, int timesSlower){
        this.render(star.getX(),star.getY(),star.getWidth(),star.getHeight(),star.getShiftY(),timesSlower);
    }

    public void render(float x,float y, float width, float height, float shiftY, int timesSlower){
        super.render(x,y+shiftY/timesSlower,width,height);
    }

}
