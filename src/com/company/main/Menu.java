package com.company.main;

import com.company.views.MenuView;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Menu extends BasicGameState{

    private static final String pathLastScore = "res/lastScore.txt";
    private static final String pathHighScore = "res/highScore.txt";
    private int lastScore = 0;
    private int highScore = 0;
    private MenuView view;

    public Menu(int state){
    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        try(Scanner scanner = new Scanner(new File(pathLastScore))) {
            while(scanner.hasNextInt()){
                lastScore=scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            File file = new File("res/lastScore.txt");
            try {
                file.createNewFile();
                System.out.println("File is created!");
                FileWriter writer = new FileWriter(file);
                writer.write("0");
                writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            lastScore = 0;
        }
        try(Scanner scanner = new Scanner(new File(pathHighScore))) {
        while(scanner.hasNextInt()){
                highScore=scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            File file = new File("res/highScore.txt");
            try {
                file.createNewFile();
                System.out.println("File is created!");
                FileWriter writer = new FileWriter(file);
                writer.write("0");
                writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            highScore = 0;
        }

        view = new MenuView(lastScore,highScore);
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        view.render(graphics);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int t) throws SlickException {

        view.update(t);

        Input keyIn = gameContainer.getInput();

        if(keyIn.isKeyDown(Input.KEY_P)){
            stateBasedGame.getState(1).init(gameContainer,stateBasedGame);
            stateBasedGame.enterState(1);
        }
        if(keyIn.isKeyDown(Input.KEY_SPACE)){
            stateBasedGame.getState(1).init(gameContainer,stateBasedGame);
            stateBasedGame.enterState(1);
        }

        if (keyIn.isKeyPressed(Input.KEY_ESCAPE)){
            AppGameContainer app = (AppGameContainer)gameContainer;
            if (gameContainer.isFullscreen()){
                app.setDisplayMode((int) (Window.width /1.5f), (int) (Window.height /1.5f), false);
            } else {
                app.setDisplayMode(Window.width, Window.height, true);
            }
        }
        if (keyIn.isKeyPressed(Input.KEY_Q)){
            gameContainer.exit();
        }
    }
}
