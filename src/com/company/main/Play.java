package com.company.main;

import com.company.controlers.Background;
import com.company.controlers.PlayerController;
import com.company.controlers.Scene;
import com.company.models.Player;
import com.company.views.PlayerView;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.*;
import java.util.Scanner;

public class Play extends BasicGameState{

    java.awt.Font awtFont;
    TrueTypeFont font;
    private static String pathLastScore = "res/lastScore.txt";
    private static String pathHighScore = "res/highScore.txt";

    public int fps = 0;

    private Player player;
    private PlayerView playerView;
    private PlayerController playerController;
    private Scene scene;

    private Background background;
    private float shiftY = 0;
    private int score = 0;
    private boolean gameOver = false;
    int highScore = 0;

    public Play(int state){
    }

    @Override
    public int getID() {
        return 1;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        awtFont = new java.awt.Font("Comic Sans MS", java.awt.Font.PLAIN, 50);
        font = new TrueTypeFont(awtFont, true);
        gameOver = false;

        player = new Player(1000,400);
        playerView = new PlayerView();
        playerController = new PlayerController(player,playerView);

        scene = new Scene();
        background = new Background();
        try(Scanner scanner = new Scanner(new File(pathHighScore))) {
            while(scanner.hasNextInt()){
                highScore=scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            File file = new File("res/highScore.txt");
            try {
                file.createNewFile();
                System.out.println("File is created!");
                FileWriter writer = new FileWriter(file);
                writer.write("0");
                writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            highScore = 0;
        }
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        background.render();
        scene.render();
        playerController.render();
        graphics.setFont(font);
        graphics.drawString("SCORE:"+score, 100,50);
        if(gameOver){
            graphics.setColor(Color.red);
            graphics.drawString("G A M E   O V E R", 700,450);
            graphics.setColor(Color.white);
        }
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int t) throws SlickException {
        Input keyIn = gameContainer.getInput();
        fps=gameContainer.getFPS();

        playerController.landedOn(scene);
        playerController.collision(scene);
        playerController.update(gameContainer,t);

        shiftY=player.getShiftY();

        scene.update(t,0,shiftY,score);
        background.update(t,0,shiftY);

        score = ((int)shiftY)/Scene.SPACE_Y;
        if(player.getY()>1080+200){
            gameOver=true;
        }

        if(player.getY()>1080+2000){
            try(BufferedWriter writer = new BufferedWriter(new FileWriter(pathLastScore))){
                writer.write(Integer.toString(score));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(score>highScore){
                try(BufferedWriter writer = new BufferedWriter(new FileWriter(pathHighScore))){
                    writer.write(Integer.toString(score));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            stateBasedGame.getState(0).init(gameContainer,stateBasedGame);
            stateBasedGame.enterState(0);
        }

        if (keyIn.isKeyPressed(Input.KEY_ESCAPE)){
            AppGameContainer app = (AppGameContainer)gameContainer;
            if (gameContainer.isFullscreen()){
                app.setDisplayMode((int) (Window.width /1.5f), (int) (Window.height /1.5f), false);
            } else {
                app.setDisplayMode(Window.width, Window.height, true);
            }
        }

    }
}
