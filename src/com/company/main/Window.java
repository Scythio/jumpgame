package com.company.main;

public class Window {
    public static int width = 1280;
    public static int height = 720;

    public Window(int width, int height){
        Window.width = width;
        Window.height = height;
    }

    public static void setWidth(int width){
        Window.width = width;
    }
    public static void setHeight(int height){
        Window.height = height;
    }

}
