package com.company.main;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Game extends StateBasedGame{

    private static final  String GAME_NAME = "JumpGame";
    private static final int MENU = 0;
    private static final int PLAY = 1;
    private static final int NORMAL_WIDTH = 1920;
    private static final int NORMAL_HEIGHT = 1080;

    public Game(String gameName){
        super(gameName);
        this.addState(new Menu(MENU));
        this.addState(new Play(PLAY));
    }

    @Override
    public void initStatesList(GameContainer gc) {
        this.enterState(MENU);
    }

    public static void main(String[] args) {
	    AppGameContainer appgc;
	    try {
            appgc = new AppGameContainer(new ScalableGame(new Game(GAME_NAME),NORMAL_WIDTH,NORMAL_HEIGHT));
            DisplayMode currentDM = Display.getDesktopDisplayMode();
            Window.setWidth(currentDM.getWidth());
            Window.setHeight(currentDM.getHeight());
            appgc.setDisplayMode(Window.width, Window.height,true);
            appgc.setIcon("res/32x32.png");
            appgc.setShowFPS(false);
	        appgc.start();
        }catch (SlickException e){
	        e.printStackTrace();
        }
    }
}