package com.company.models;

import org.newdawn.slick.SlickException;

public class Star extends Structure {

    public Star(float x, float y, float width, float height, float startShiftY) throws SlickException {
        super(x, y, width, height, 0, startShiftY);
    }

}
