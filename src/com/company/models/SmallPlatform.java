package com.company.models;

import com.company.models.Platform;

public class SmallPlatform extends Platform {
    public static float WIDTH = 60;
    public static float HEIGHT = 60;
    public static final Type TYPE = Type.SMALL_PLATFORM;
    public SmallPlatform(float x, float y, float startShiftY){
        super(x, y, WIDTH, HEIGHT, startShiftY, TYPE);
    }
}
