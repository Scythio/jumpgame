package com.company.models;

public class BigPlatform extends Platform {
    public static final int WIDTH = 120;
    public static final int HEIGHT = 60;
    public static final Type TYPE = Type.BIG_PLATFORM;
    public BigPlatform(float x, float y, float startShiftY){
        super(x, y, WIDTH, HEIGHT, startShiftY, TYPE);
    }
}
