package com.company.models;

import com.company.controlers.Probability;
import com.company.controlers.Scene;

public class MovingPlatform extends Platform {

    public static final int WIDTH = 120;
    public static final int HEIGHT = 60;
    public static final Type TYPE = Type.MOVING_PLATFORM;

    private static float V=0.2f;
    private float velocityX = V;

    public MovingPlatform(float x, float y, float startShiftY){
        super(x, y, WIDTH, HEIGHT, startShiftY, TYPE);
        if(Probability.of(50)){
            velocityX =-V;
        }
    }
    public MovingPlatform(float x, float y, float width, float height, float startShiftY, Type type){
        super(x, y, width, height, startShiftY, type);
        if(Probability.of(50)){
            velocityX =-V;
        }
    }

    public void update(int t, float shiftX, float shiftY){
        super.update(shiftX,shiftY);
        if(this.getX()<0){
            this.setX(0);
            this.setBounce(true);
        }
        if(this.getX()+this.getWidth()>1920){
            this.setX(1920-this.getWidth());
            this.setBounce(true);
        }
        if(this.isBounce()){
            if(velocityX>0){
                velocityX = -V;
            }
            else{
                velocityX = V;
            }
            this.setBounce(false);
        }
        this.setX(this.getX()+velocityX * t);
    }

    public boolean collision(Platform platform){
        if(this.getY()+this.getShiftY()<1080){
            if(platform.getY()+platform.getShiftY()>this.getY()+this.getShiftY()-10  &&  platform.getY()+platform.getShiftY()<this.getY()+this.getShiftY()+10){
                if(this.velocityX<0){
                    if(this.getX()>platform.getX()  &&  this.getX()<platform.getX()+platform.getWidth()){
                        this.setX(this.getX()+platform.getX()+platform.getWidth()-this.getX());
                        return true;
                    }
                }
                else {
                    if(this.getX()+this.getWidth()<platform.getX()+platform.getWidth()  &&  this.getX()+this.getWidth()>platform.getX()){
                        //this.x -=this.x+this.width-platform.x;
                        this.setX(platform.getX()-this.getWidth());
                        return true;
                    }
                }
            }
        }
        return  false;
    }
    public boolean collision(Scene scene){
        for(Platform platform: scene.manyPlatforms) {
            if(platform!=null){
                if(this.collision(platform)) {
                    platform.setBounce(true);
                    this.setBounce(true);
                    return true;
                }
            }
        }
        return false;
    }
}
