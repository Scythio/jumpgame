package com.company.models;

import com.company.controlers.PlatformInterface;

public class Trampoline extends Platform implements PlatformInterface {

    public static final int WIDTH = 120;
    public static final int HEIGHT = 90;
    private Theme theme = Theme.MIX;


    public Trampoline(float x, float y, float startShiftY){
        super(x, y, WIDTH, HEIGHT, startShiftY, Type.MIX_TRAMPOLINE);
    }

    public Trampoline(float x, float y, float startShiftY, Theme theme){
        this(x, y, startShiftY);
        this.setTheme(theme);
        switch (theme) {
            case MIX:
                setType(Type.MIX_TRAMPOLINE);
                break;
            case SOLID:
                setType(Type.SOLID_TRAMPOLINE);
                break;
            case MOVING:
                setType(Type.MOVING_TRAMPOLINE);
                break;
            case BREAKABLE:
                setType(Type.BREAKABLE_TRAMPOLINE);
                break;
            default:
                setType(Type.MIX_TRAMPOLINE);
                break;
        }
    }

    public Theme getTheme() {
        return theme;
    }
    public void setTheme(Theme theme) {
        this.theme = theme;
    }
}
