package com.company.models;

public class Structure{

    private float x, y;
    private float width;
    private float height;
    private float shiftX, shiftY;
    private float startShiftX=0, startShiftY=0;
    private boolean toBeRemoved = false;
    public float getX(){
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY(){
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    public float getHeight() {
        return height;
    }
    public float getWidth() {
        return width;
    }
    public float getShiftY() {
        return shiftY;
    }
    public void setToBeRemoved(boolean toBeRemoved) { this.toBeRemoved = toBeRemoved; }
    public boolean isToBeRemoved() { return toBeRemoved; }

    public Structure(float x, float y, float width, float height){
        this.x =x;
        this.y =y;
        this.width=width;
        this.height=height;
    }
    public Structure(float x, float y, float width, float height, float startShiftX, float startShiftY){
        this.x =x;
        this.y =y;
        this.width=width;
        this.height=height;
        this.startShiftY=startShiftY;
        this.startShiftX=startShiftX;
    }

    public void update(float shiftX, float shiftY){
        this.shiftX=shiftX-startShiftX;
        this.shiftY=shiftY-startShiftY;
    }
}
