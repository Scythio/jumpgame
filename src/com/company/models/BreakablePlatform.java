package com.company.models;

import org.newdawn.slick.SlickException;

public class BreakablePlatform extends Platform {

    public static final int WIDTH = 120;
    public static final int HEIGHT = 60;
    private State state = State.THREE_LAYERS;

    public State getState() {
        return state;
    }
    public void setState(State state) {
        this.state = state;
    }

    public BreakablePlatform(float x, float y, float startShiftY) throws SlickException {
        super(x, y, WIDTH, HEIGHT, startShiftY, Type.BREAKABLE_PLATFORM_THREE_LAYERS);
    }

    public BreakablePlatform(float x, float y, float width, float height, float startShiftY, State state, Type type) throws SlickException {
        super(x, y, width, height, startShiftY, type);
        this.setState(state);
    }

    public BreakablePlatform(float x, float y, float startShiftY, State state){
        super(x, y, WIDTH, HEIGHT, startShiftY, Type.BREAKABLE_PLATFORM_THREE_LAYERS);
        this.setState(state);
        switch(state){
            case THREE_LAYERS:
                this.setType(Type.BREAKABLE_PLATFORM_THREE_LAYERS);
                break;
            case TWO_LAYERS:
                this.setType(Type.BREAKABLE_PLATFORM_TWO_LAYERS);
                break;
            case ONE_LAYER:
                this.setType(Type.BREAKABLE_PLATFORM_ONE_LAYER);
                break;
            default:
                this.setType(Type.BREAKABLE_PLATFORM_ONE_LAYER);
                this.setToBeRemoved(true);
                break;
        }
    }

    public void update(int t, float shiftX, float shiftY){
        super.update(shiftX, shiftY);
        if(this.isLandedOn()){
            breakPlatform();
            setLandedOn(false);
        }
        if(this.getState()==State.BROKEN){
            this.setToBeRemoved(true);
        }
        updateType();
    }

    public void updateType(){
        switch(state) {
            case THREE_LAYERS:
                this.setType(Type.BREAKABLE_PLATFORM_THREE_LAYERS);
                break;
            case TWO_LAYERS:
                this.setType(Type.BREAKABLE_PLATFORM_TWO_LAYERS);
                break;
            case ONE_LAYER:
                this.setType(Type.BREAKABLE_PLATFORM_ONE_LAYER);
                break;
            default:
                break;
        }
    }

    private void breakPlatform(){
        switch(state){
            case THREE_LAYERS:
                this.setState(State.TWO_LAYERS);
                break;
            case TWO_LAYERS:
                this.setState(State.ONE_LAYER);
                break;
            case ONE_LAYER:
                this.setState(State.BROKEN);
                break;
            default:
                break;
        }
    }
}