package com.company.models;

import com.company.controlers.Probability;

public class Enemy extends Structure {

    public static final float WIDTH = 100;
    public static final float HEIGHT = 100;
    private static final float V=0.4f;
    private float velocityX = V;


    public Enemy(float x, float y, float startShiftY) {
        super(x, y, WIDTH, HEIGHT, 0, startShiftY);
        if(Probability.of(50)){
            velocityX =-V;
        }
    }
    public Enemy(float x, float y, float velocityX, float startShiftY) {
        super(x, y, WIDTH, HEIGHT, 0, startShiftY);
        this.velocityX = velocityX;
    }

    public void update(int t, float shiftX, float shiftY){
        super.update(shiftX,shiftY);
        if(this.getX()<0){
            this.setX(0);
            velocityX = V;
        }
        if(this.getX()+this.getWidth()>1920){
            this.setX(1920-this.getWidth());
            velocityX = -V;
        }
        this.setX(this.getX()+velocityX * t);
    }

    public float getVelocityX() {
        return velocityX;
    }
}

