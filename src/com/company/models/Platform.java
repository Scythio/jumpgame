package com.company.models;

import com.company.controlers.PlatformInterface;
import com.company.controlers.Scene;

public class Platform extends Structure implements PlatformInterface {

    private Type type;
    private State state;
    private boolean bounce = false;
    private boolean landedOn = false;

    public Platform(float x, float y, float width, float height, float startShiftY, Type type) {
        super(x, y, width, height, 0, startShiftY);
        this.type = type;
    }

    public void update(int t, float shiftX, float shiftY) {
        super.update(shiftX,shiftY);
    }

    public boolean collision(Structure structure){
        return  false;
    }

    public boolean collision(Scene scene){
        return  false;
    }

    public void setBounce(boolean bounce){ this.bounce = bounce; }
    public void setLandedOn(boolean landedOn) {
        this.landedOn = landedOn;
    }
    public Theme getTheme(){ return null; }
    public Type getType(){
        return this.type;
    }
    public void setType(Type type){
        this.type = type;
    }
    public boolean isBounce() {
        return bounce;
    }
    public boolean isLandedOn() {
        return landedOn;
    }
}
