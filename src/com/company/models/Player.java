package com.company.models;

public class Player extends Structure {

    private final static float CAMERA_HEIGHT = 540;
    private float shiftY;
    private float velocityY=0;
    private float velocityX=0;
    private float alpha=0;
    private static final float  omega=0.2f;
    private static final float  velocityYMax=1.15f, accelerationY=0.0024f;
    private static final float  velocityXMax=1.2f, velocityXMin=0.001f, accelerationX=0.005f, decelerationX=0.001f;
    private boolean jump=false;
    private boolean superJump=false;

    private boolean ignoreCollision = false;
    private boolean shieldOn = false;
    private boolean hit = false;

    private boolean goingDown = false;
    private boolean goingUp = false;
    private boolean goingNotUpNorDown = true;

    private boolean goLeft = false;
    private boolean goRight = false;
    private boolean dashDown = false;

    public void setVelocityY(float velocityY) {
        this.velocityY = velocityY;
    }
    public float getVelocityY() { return velocityY; }
    public void setVelocityX(float velocityX) {
        this.velocityX = velocityX;
    }
    public float getVelocityX() {
        return velocityX;
    }
    public void setHit(boolean hit) {
        this.hit = hit;
    }
    public void setIgnoreCollision(boolean ignoreCollision) {
        this.ignoreCollision = ignoreCollision;
    }
    public boolean isIgnoreCollision() {
        return ignoreCollision;
    }
    public boolean isShieldOn() {
        return shieldOn;
    }
    public boolean isGoingNotUpNorDown() {
        return goingNotUpNorDown;
    }
    public boolean isGoingUp() {
        return goingUp;
    }
    public boolean isGoingDown() {
        return goingDown;
    }
    public void setGoLeft(boolean goingUp) {
        this.goLeft = goingUp;
    }
    public void setGoRight(boolean goingDown) {
        this.goRight = goingDown;
    }
    public void setDashDown(boolean dashDown) {
        this.dashDown = dashDown;
    }
    public void setJump(boolean jump) {
        this.jump = jump;
    }
    public void setSuperJump(boolean superJump) {
        this.superJump = superJump;
    }
    public boolean isHit() {
        return hit;
    }
    public float getAlpha() {
        return alpha;
    }

    @Override
    public float getShiftY() {
        return shiftY;
    }

    public Player(float x, float y) {
        super(x,y,60,60);
    }
    public void update(int t) {

        if(jump){
            if(dashDown){
                velocityY =-velocityYMax;
            }
            else{
                velocityY =-1.2f * velocityYMax;
            }
            jump = false;
            goingNotUpNorDown =true;
            goingDown=false;
            goingUp=false;

        }
        if(superJump){
            velocityY =-2.8f * velocityYMax;
            superJump = false;
            shieldOn=true;
            ignoreCollision=true;
        }

        if(velocityY < velocityYMax){
            if(dashDown){
                velocityY += 2.5*accelerationY * t;
            }
            else{
                velocityY += accelerationY * t;
            }
        }
        else{
            velocityY = velocityYMax;
        }
        if(this.getY() >CAMERA_HEIGHT){
            this.setY(this.getY()+velocityY * t);
        }
        else{
            if(velocityY >0){
                this.setY(this.getY()+velocityY * t);
            }
            else{
                shiftY -= velocityY * t;
            }
        }

        //horizontal velocity and position
        if(velocityX!=0){
            if(velocityX>0){
                if(velocityX<velocityXMin){
                    velocityX=0;
                }
                else{
                    if(dashDown){
                        velocityX -= 2*decelerationX*t;
                    }
                    else{
                        velocityX -= decelerationX*t;
                    }
                    if(this.getX() +this.getWidth()<1920){
                        this.setX(this.getX()+velocityX*t);
                    }
                    else {
                        this.setX(1920-this.getWidth());
                        velocityX=-velocityX;
                    }
                }
            }
            else{
                if(velocityX>-velocityXMin){
                    velocityX=0;
                }
                else{
                    if(dashDown){
                        velocityX += 2*decelerationX*t;
                    }
                    else{
                        velocityX += decelerationX*t;
                    }
                    if(this.getX()>0){
                        this.setX(this.getX()+velocityX*t);
                    }
                    else{
                        this.setX(0);
                        velocityX=-velocityX;
                    }
                }

            }
        }
        if(hit){
            alpha += omega *t;
        }

        if(velocityY>0){
            shieldOn=false;
            if(!hit){
                ignoreCollision=false;
            }
            if(velocityY<velocityYMax/6){
                goingNotUpNorDown =true;
                goingDown=false;
                goingUp=false;
            }
            else{
                goingNotUpNorDown =false;
                goingDown=true;
                goingUp=false;
            }
        }
        else{
            if(velocityY>-velocityYMax/6){
                goingNotUpNorDown =true;
                goingDown=false;
                goingUp=false;
            }
            else{
                goingNotUpNorDown =false;
                goingDown=false;
                goingUp=true;
            }
        }

        if(goLeft){
            if(velocityX==0){
                velocityX=-velocityXMin;
            }
            if(velocityX > -velocityXMax){
                if(dashDown){
                    velocityX -= 0.75*accelerationX*t;
                }
                else{
                    velocityX -= accelerationX*t;
                }
            }
            else{
                velocityX = -velocityXMax;
            }
        }
        if(goRight) {
            if(velocityX==0){
                velocityX=velocityXMin;
            }
            if(velocityX < velocityXMax){
                if(dashDown){
                    velocityX += 0.75*accelerationX*t;
                }
                else{
                    velocityX += accelerationX*t;
                }
            }
            else{
                velocityX = velocityXMax;
            }
        }
    }
}
