package com.company.models;

import com.company.models.MovingPlatform;

public class SmallMovingPlatform extends MovingPlatform {

    public static final int WIDTH = 60;
    public static final int HEIGHT = 60;
    public static final Type TYPE = Type.SMALL_MOVING_PLATFORM;

    public SmallMovingPlatform(float x, float y, float startShiftY){
        super(x, y, WIDTH, HEIGHT, startShiftY, TYPE);
    }
}