package com.company.models;

import com.company.models.BreakablePlatform;
import org.newdawn.slick.SlickException;

public class SmallBreakablePlatform extends BreakablePlatform {
    public static final int WIDTH = 60;
    public static final int HEIGHT = 60;
    public static final Type TYPE2 = Type.SMALL_BREAKABLE_PLATFORM_TWO_LAYERS;
    public static final Type TYPE1 = Type.SMALL_BREAKABLE_PLATFORM_ONE_LAYER;

    public SmallBreakablePlatform(float x, float y, float startShiftY) throws SlickException {
        super(x, y, WIDTH, HEIGHT, startShiftY,State.TWO_LAYERS, TYPE2);
    }

    public SmallBreakablePlatform(float x, float y, float startShiftY, State state) throws SlickException {
        super(x, y, WIDTH, HEIGHT, startShiftY, State.TWO_LAYERS, TYPE2);
        this.setState(state);
        switch(state){
            case TWO_LAYERS:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_TWO_LAYERS);
                break;
            case ONE_LAYER:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_ONE_LAYER);
                break;
            default:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_ONE_LAYER);
                setToBeRemoved(true);
                break;
        }
    }

    @Override
    public void updateType(){
        switch(this.getState()) {
            case THREE_LAYERS:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_TWO_LAYERS);
                this.setState(State.TWO_LAYERS);
                break;
            case TWO_LAYERS:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_TWO_LAYERS);
                break;
            case ONE_LAYER:
                this.setType(Type.SMALL_BREAKABLE_PLATFORM_ONE_LAYER);
                break;
            default:
                break;
        }
    }
}