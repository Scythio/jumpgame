package com.company.controlers;
import com.company.models.Star;
import com.company.views.StarView;
import org.newdawn.slick.SlickException;
import java.util.ArrayList;
import java.util.Iterator;

public class Background {

    private ArrayList<Star> stars1, stars2, stars3;
    private StarView starView;
    private float currentShiftY=0, lastShiftY=0;
    private final float SPACE_Y = 480;

    public Background() throws SlickException {
        stars1 = new ArrayList<Star>(60);
        stars2 = new ArrayList<Star>(180);
        stars3 = new ArrayList<Star>(340);
        starView = new StarView();
        int x, y, size;
        for(int i = 0; i < 20; i++){
            x = (int) (Math.random() * 1920);
            y = (int) (Math.random() * 1080);
            size = (int) (20 + Math.random() * 10);
            stars1.add(new Star(x,y,size,size,0));
        }
        for(int i = 0; i < 50; i++){
            x = (int) (Math.random() * 1920);
            y = (int) (Math.random() * 1080);
            size = (int) (10 + Math.random() * 10);
            stars2.add(new Star(x,y,size,size,0));
        }
        for(int i = 0; i < 100; i++){
            x = (int) (Math.random() * 1920);
            y = (int) (Math.random() * 1080);
            size = (int) (5 + Math.random() * 10);
            stars3.add(new Star(x,y,size,size,0));
        }
    }

    public void render() {
        for(Star star: stars1) {
            if(star!=null){
                starView.render(star,4);
            }
        }
        for(Star star2: stars2) {
            if(star2!=null){
                starView.render(star2,6);
            }
        }
        for(Star star3: stars3) {
            if(star3!=null){
                starView.render(star3,8);
            }
        }
    }
    public void update(int t, float shiftX, float shiftY) throws SlickException {

        currentShiftY = shiftY - lastShiftY;

        if(currentShiftY>=SPACE_Y) {
            currentShiftY = 0;
            lastShiftY = shiftY;
            int x, y, size;
            for(int i = 0; i < 4; i++){
                x = (int) (Math.random() * 1920);
                y = (int) -(Math.random() * SPACE_Y)-20;
                size = (int) (20 + Math.random() * 10);
                stars1.add(new Star(x,y,size,size,shiftY));
            }
            for(int i = 0; i < 8; i++){
                x = (int) (Math.random() * 1920);
                y = (int) -(Math.random() * SPACE_Y)-20;
                size = (int) (10 + Math.random() * 10);
                stars2.add(new Star(x,y,size,size,shiftY));
            }
            for(int i = 0; i < 12; i++){
                x = (int) (Math.random() * 1920);
                y = (int) -(Math.random() * SPACE_Y)-20;
                size = (int) (5 + Math.random() * 10);
                stars3.add(new Star(x,y,size,size,shiftY));
            }
        }

        Iterator<Star> iterator = stars1.iterator();

        while(iterator.hasNext()){
            Star star = iterator.next();
            star.update(0,shiftY);
            if(star.getY()+star.getShiftY()>1080*4+SPACE_Y*4){
                iterator.remove();
            }
        }

        iterator = stars2.iterator();
        while(iterator.hasNext()){
            Star star = iterator.next();
            star.update(0,shiftY);
            if(star.getY()+star.getShiftY()>1080*6+SPACE_Y*6){
                iterator.remove();
            }
        }

        iterator = stars3.iterator();
        while(iterator.hasNext()){
            Star star = iterator.next();
            star.update(0,shiftY);
            if(star.getY()+star.getShiftY()>1080*8+SPACE_Y*8){
                iterator.remove();
            }
        }
    }
}
