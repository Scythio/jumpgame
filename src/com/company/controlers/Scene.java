package com.company.controlers;

import com.company.models.*;
import com.company.views.EnemyView;
import com.company.views.PlatformView;
import org.newdawn.slick.SlickException;
import java.util.ArrayList;
import java.util.Iterator;

public class Scene implements PlatformInterface {
    public static final int ROWS = 4, COLUMNS = 8;
    public static final int SPACE_X = 1920/(COLUMNS), SPACE_Y = 1080/(ROWS);
    public static final float HIGH_PROBABILITY=25;

    private float currentShiftY=0, lastShiftY=0;
    private int rowsInCurrentLayer = 0;
    private boolean nowGap;
    private boolean lastOneInMiddle = false;
    private boolean latelyWasEnemy = false;
    private Theme theme = Theme.MIX;
    private enum Difficulty {EASY, MEDIUM, HARD}
    private Difficulty dif = Difficulty.EASY;

    public ArrayList<Platform> manyPlatforms;
    public ArrayList<Enemy> manyEnemies;
    private PlatformView platformView;
    private EnemyView enemyView;

    public Scene() throws SlickException {
        manyPlatforms = new ArrayList<>();
        manyEnemies = new ArrayList<>();
        platformView = new PlatformView();
        enemyView = new EnemyView();
        startSet();
    }

    private void addRandomPlatform(int x, int y, float startShiftY) throws SlickException {
        if(Probability.of(50)){
            if(Probability.of(70)){
                manyPlatforms.add(new BigPlatform(x,y,startShiftY));
            }
            else{
                manyPlatforms.add(new SmallPlatform(x,y,startShiftY));
            }
        }else{
            if(Probability.of(0)){
                if(Probability.of(70)){
                    manyPlatforms.add(new MovingPlatform(x,y,startShiftY));
                }
                else{
                    manyPlatforms.add(new SmallMovingPlatform(x,y,startShiftY));
                }
            }
            else{
                if(Probability.of(70)){
                    if(Probability.of(60)){
                        manyPlatforms.add(new BreakablePlatform(x,y,startShiftY));
                    }
                    else{
                        if(Probability.of(60)){
                            manyPlatforms.add(new BreakablePlatform(x,y,startShiftY,State.TWO_LAYERS));
                        }
                        else{
                            manyPlatforms.add(new BreakablePlatform(x,y,startShiftY,State.ONE_LAYER));
                        }
                    }
                }
                else{
                    if(Probability.of(60)){
                        manyPlatforms.add(new SmallBreakablePlatform(x,y,startShiftY));
                    }
                    else{
                        manyPlatforms.add(new SmallBreakablePlatform(x,y,startShiftY,State.ONE_LAYER));
                    }
                }
            }

        }
    }

    private void addRandomPlatform(int x, int y, float startShiftY, float pSolid, float pMoving, float pBig, float pIntact) throws SlickException {
        if(Probability.of(pSolid)){
            if(Probability.of(pBig)){
                manyPlatforms.add(new BigPlatform(x,y,startShiftY));
            }
            else{
                manyPlatforms.add(new SmallPlatform(x,y,startShiftY));
            }
        }else{
            if(Probability.of(pMoving)){
                if(Probability.of(pBig)){
                    manyPlatforms.add(new MovingPlatform(x,y,startShiftY));
                }
                else{
                    manyPlatforms.add(new SmallMovingPlatform(x,y,startShiftY));
                }
            }
            else{
                if(Probability.of(pBig)){
                    if(Probability.of(pIntact)){
                        manyPlatforms.add(new BreakablePlatform(x,y,startShiftY));
                    }
                    else{
                        if(Probability.of(pIntact)){
                            manyPlatforms.add(new BreakablePlatform(x,y,startShiftY,State.TWO_LAYERS));
                        }
                        else{
                            manyPlatforms.add(new BreakablePlatform(x,y,startShiftY,State.ONE_LAYER));
                        }
                    }
                }
                else{
                    if(Probability.of(pIntact)){
                        manyPlatforms.add(new SmallBreakablePlatform(x,y,startShiftY));
                    }
                    else{
                        manyPlatforms.add(new SmallBreakablePlatform(x,y,startShiftY,State.ONE_LAYER));
                    }
                }
            }
        }
    }

    private void startSet() throws SlickException {
        int yy = (int)(1080-SPACE_Y/1.1);
        addFlor(yy,0);
        yy-=SPACE_Y;
        for (int y = yy; y > -SPACE_Y; y-=SPACE_Y) {
            if(lastOneInMiddle){
                lastOneInMiddle=addRow(y,HIGH_PROBABILITY,0,false);
            }
            else{
                lastOneInMiddle=addRow(y,HIGH_PROBABILITY,0,true);
            }
        }
    }
    private void addFlor(float y, float startShiftY) {
        for(int x = 0; x < 1920; x += BigPlatform.WIDTH){
            manyPlatforms.add(new BigPlatform(x,y,startShiftY));
        }
    }
    private void addTrampolines(float y, float startShiftY) {
        Theme type1 = randomType();
        Theme type2;
        do{
            type2 = randomType();
        }
        while(type1==type2);
        manyPlatforms.add(new Trampoline((int)(1920/4),y,startShiftY,type1));
        manyPlatforms.add(new Trampoline((int)(1920/1.5),y,startShiftY,type2));
    }
    private Theme randomType(){
        double randomValue = Math.random()*100;
        if(randomValue<25){
            return Theme.MIX;
        }
        if(randomValue<50){
            return Theme.SOLID;
        }
        if(randomValue<75){
            return Theme.MOVING;
        }
        if(randomValue<100){
            return Theme.BREAKABLE;
        }
        else{
            return Theme.MIX;
        }
    }

    public void render(){
        for(Platform platform: manyPlatforms) {
            if(platform!=null){
                platformView.render(platform);
            }
        }
        for(Enemy enemy: manyEnemies) {
            if (enemy != null) {
                enemyView.render(enemy);
            }
        }
    }
    public void update(int t, float shiftX, float shiftY, int score) throws SlickException {

        if(score<30){
            dif = Difficulty.EASY;
        }
        else if(score<60){
            dif = Difficulty.MEDIUM;
        }
        else{
            dif = Difficulty.HARD;
        }
        sceneBuilder(shiftY,theme,dif);

        for(Platform platform: manyPlatforms){
            if(platform!=null){
                platform.collision(this);
                if(platform.getClass()==Trampoline.class){
                    if(platform.isLandedOn()){
                        theme=platform.getTheme();
                    }
                }
            }
        }

        Iterator<Platform> iterator = manyPlatforms.iterator();
        while(iterator.hasNext()){
            Platform platform = iterator.next();
            platform.update(t,shiftX,shiftY);
            if(platform.isToBeRemoved()){
                iterator.remove();
            }
            if(platform.getY()+platform.getShiftY()>1080){
                iterator.remove();
            }
        }

        if(!manyEnemies.isEmpty()){
            Iterator<Enemy> eIterator = manyEnemies.iterator();
            while(eIterator.hasNext()){
                Enemy enemy = eIterator.next();
                enemy.update(t,shiftX,shiftY);
                if(enemy.isToBeRemoved()){
                    eIterator.remove();
                }
                if(enemy.getY()+enemy.getShiftY()>1080){
                    eIterator.remove();
                }
            }
        }

    }

    private void generator(float shiftY, float pMany, float pSolid, float pMoving, float pBig, float pIntact, float pEnemy) throws SlickException {
        currentShiftY = shiftY - lastShiftY;
        if(currentShiftY>=SPACE_Y){
            currentShiftY=0;
            lastShiftY=shiftY;
            rowsInCurrentLayer += 1;
            if(nowGap){
                if(rowsInCurrentLayer>4){
                    rowsInCurrentLayer = 0;
                    nowGap = false;
                }
            }
            else{
                if(rowsInCurrentLayer>10){
                    addTrampolines(-SPACE_Y,shiftY);
                    rowsInCurrentLayer = 0;
                    nowGap = true;
                }
                else{
                    if(lastOneInMiddle){
                        lastOneInMiddle=addRow(-SPACE_Y,shiftY,pMany,pSolid,pMoving,pBig,pIntact,false);
                    }
                    else{
                        lastOneInMiddle=addRow(-SPACE_Y,shiftY,pMany,pSolid,pMoving,pBig,pIntact,true);
                    }
                    if(Probability.of(pEnemy)){
                        addEnemy(-SPACE_Y/2-20,shiftY);
                        latelyWasEnemy = true;
                    }
                }
            }
        }
    }


    private void sceneBuilder(float shiftY, Theme type, Difficulty difficulty) throws SlickException {
        switch(difficulty){
            case EASY:
                switch(type){
                    case MIX:
                        generator(shiftY,50,60,60,75,100,0);
                        break;
                    case SOLID:
                        generator(shiftY,40,100,0,75,100,20);
                        break;
                    case MOVING:
                        generator(shiftY,30,0,100,75,100,0);
                        break;
                    case BREAKABLE:
                        generator(shiftY,40,0,0,75,60,0);
                        break;
                    default:
                        generator(shiftY,50,50,50,50,50,0);
                        break;
                }
                break;
            case MEDIUM:
                switch(type){
                    case MIX:
                        generator(shiftY,30,30,60,60,80,0);
                        break;
                    case SOLID:
                        generator(shiftY,30,100,0,75,100,35);
                        break;
                    case MOVING:
                        generator(shiftY,25,0,100,50,100,0);
                        break;
                    case BREAKABLE:
                        generator(shiftY,20,0,0,60,50,0);
                        break;
                    default:
                        generator(shiftY,25,50,50,50,50,0);
                        break;
                }
                break;
            case HARD:
                switch(type){
                    case MIX:
                        generator(shiftY,20,30,60,50,60,10);
                        break;
                    case SOLID:
                        generator(shiftY,20,100,0,50,100,60);
                        break;
                    case MOVING:
                        generator(shiftY,20,0,100,40,100,5);
                        break;
                    case BREAKABLE:
                        generator(shiftY,15,0,0,40,40,5);
                        break;
                    default:
                        generator(shiftY,25,50,50,50,50,20);
                        break;
                }
                break;
            default:
                generator(shiftY,25,50,50,50,50,20);
                break;
        }
    }

    private boolean addRow(int y, float probabilityInPercents, float startShiftY, boolean putOneInMiddle) throws SlickException {
        int numberOfPlatforms = 0;
        boolean oneInMiddle = false;
        int manyX[] = new int[COLUMNS];
        if(putOneInMiddle){
            do{
                numberOfPlatforms=0;
                int i=0;
                for(int x = SPACE_X/3; x < 1920; x+=SPACE_X){
                    if(Probability.of(probabilityInPercents)) {
                        manyX[i] = x;
                        if (x > 1920 / 3 && x < 1920 / 1.5f) {
                            oneInMiddle = true;
                        }
                        ++numberOfPlatforms;
                        ++i;
                    }
                }
            }while(!oneInMiddle || numberOfPlatforms<2);
            for (int i=0;i<numberOfPlatforms;i++){
                addRandomPlatform(manyX[i],y,startShiftY);
            }
        }
        else{
            do{
                numberOfPlatforms=0;
                int i=0;
                for(int x = SPACE_X/3; x < 1920; x+=SPACE_X){
                    if(Probability.of(probabilityInPercents)){
                        manyX[i] = x;
                        if(x>1920/3 && x<1920/1.5f){
                            oneInMiddle=true;
                        }
                        ++numberOfPlatforms;
                        ++i;
                    }
                }
            }while(numberOfPlatforms<2);
            for (int i=0;i<numberOfPlatforms;i++){
                addRandomPlatform(manyX[i],y,startShiftY);
            }
        }
        return  oneInMiddle;

    }

    private boolean addRow(int y, float startShiftY, float pMany, float pSolid, float pMoving, float pBig, float pIntact, boolean putOneInMiddle) throws SlickException {
        int numberOfPlatforms = 0;
        boolean oneInMiddle = false;
        int manyX[] = new int[COLUMNS];
        if(putOneInMiddle){
            do{
                numberOfPlatforms=0;
                int i=0;
                for(int x = SPACE_X/3; x < 1920; x+=SPACE_X){
                    if(Probability.of(pMany)) {
                        manyX[i] = x;
                        if (x > 1920 / 3 && x < 1920 / 1.5f) {
                            oneInMiddle = true;
                        }
                        ++numberOfPlatforms;
                        ++i;
                    }
                }
            }while(!oneInMiddle || numberOfPlatforms<2);
            for (int i=0;i<numberOfPlatforms;i++){
                addRandomPlatform(manyX[i],y,startShiftY,pSolid,pMoving,pBig,pIntact);
            }
        }
        else{
            do{
                numberOfPlatforms=0;
                int i=0;
                for(int x = SPACE_X/3; x < 1920; x+=SPACE_X){
                    if(Probability.of(pMany)){
                        manyX[i] = x;
                        if(x>1920/3 && x<1920/1.5f){
                            oneInMiddle=true;
                        }
                        ++numberOfPlatforms;
                        ++i;
                    }
                }
            }while(numberOfPlatforms<2);
            for (int i=0;i<numberOfPlatforms;i++){
                addRandomPlatform(manyX[i],y,startShiftY,pSolid,pMoving,pBig,pIntact);
            }
        }
        return  oneInMiddle;

    }

    private boolean addEnemy(int y, float startShiftY) {
        int x;
        if(!manyEnemies.isEmpty()){
            Enemy lastEnemy = manyEnemies.get(manyEnemies.size()-1);
            if(lastEnemy.getShiftY() > 2*SPACE_Y){
                x = (int) (Math.random()*1000);
                manyEnemies.add(new Enemy(x,y,startShiftY));
            }
            else{
                if(lastEnemy.getX()<(1920-Enemy.WIDTH)/2){
                    x = (int)( lastEnemy.getX() + (1920-Enemy.WIDTH)/2 );
                }
                else{
                    x = (int)( lastEnemy.getX() - (1920-Enemy.WIDTH)/2 );
                }
                manyEnemies.add(new Enemy(x,y,-lastEnemy.getVelocityX(),startShiftY));
            }
        }
        else{
            x = (int) (Math.random()*1000);
            manyEnemies.add(new Enemy(x,y,startShiftY));
        }
        return true;
    }
}