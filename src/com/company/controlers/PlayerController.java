package com.company.controlers;
import com.company.models.*;
import com.company.views.PlayerView;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public class PlayerController {

    private Player model;
    private PlayerView view;

    public PlayerController(Player model, PlayerView view){
        this.model = model;
        this.view = view;
    }

    public void update(GameContainer gameContainer, int t){

        Input keyIn = gameContainer.getInput();

        model.setDashDown(keyIn.isKeyDown(Input.KEY_DOWN));
        model.setGoLeft(keyIn.isKeyDown(Input.KEY_LEFT));
        model.setGoRight(keyIn.isKeyDown(Input.KEY_RIGHT));

        model.update(t);
    }

    public void render(){
        if(model.isGoingNotUpNorDown()){
            view.render(model.getX(),model.getY(),1,false);
        }
        else{
            if(model.isGoingDown()){
                view.render(model.getX(),model.getY(),2,false);
            }
            else{
                view.render(model.getX(),model.getY(),3,model.isShieldOn());
            }
        }
        if(model.isHit()){
            view.setRotation(model.getAlpha());
        }
    }

    public boolean collision(Structure structure){
        if(!model.isIgnoreCollision()){
            if((model.getX()>structure.getX()  &&  model.getX()<structure.getX()+structure.getWidth()) || (model.getX()+model.getWidth()<structure.getX()+structure.getWidth()  &&  model.getX()+model.getWidth()>structure.getX())){
                if(((model.getY()>structure.getY()+structure.getShiftY()  &&  model.getY()<structure.getY()+structure.getShiftY()+structure.getHeight())) || (model.getY()+model.getHeight()>structure.getY()+structure.getShiftY() && model.getY()+model.getHeight()<structure.getY()+structure.getShiftY()+structure.getHeight())){
                    return true;
                }
            }
        }
        return  false;
    }

    public boolean landedOn(Structure structure){
        if(!model.isIgnoreCollision()){
            if(model.getVelocityY()>0){
                if((model.getX()>structure.getX()  &&  model.getX()<structure.getX()+structure.getWidth()) || (model.getX()+model.getWidth()<structure.getX()+structure.getWidth()  &&  model.getX()+model.getWidth()>structure.getX())){
                    if(model.getY()+model.getHeight()>structure.getY()+structure.getShiftY()-20 && model.getY()+model.getHeight()<structure.getY()+structure.getShiftY()+10){
                        return true;
                    }
                }
            }
        }
        return  false;
    }
    public boolean landedOn(Scene scene){
        for(Platform platform: scene.manyPlatforms) {
            if(platform!=null){
                if(this.landedOn(platform)) {
                    platform.setLandedOn(true);
                    model.setJump(true);
                    if(platform.getClass()== Trampoline.class){
                        model.setSuperJump(true);
                    }
                    return true;
                }
            }
        }
        return false;
    }
    public boolean collision(Scene scene){
        for(Enemy enemy: scene.manyEnemies) {
            if(enemy!=null){
                if(this.collision(enemy)) {
                    model.setIgnoreCollision(true);
                    model.setHit(true);
                    model.setVelocityY(-model.getVelocityY());
                    model.setVelocityX(-model.getVelocityX());
                    return true;
                }
            }
        }
        return false;
    }
}
