package com.company.controlers;

public class Probability {
    public static boolean of(float ProbabilityInPercents) {
        double randomValue = Math.random()*100;  //0.0 to 99.9
        return randomValue <= ProbabilityInPercents;
    }
}
